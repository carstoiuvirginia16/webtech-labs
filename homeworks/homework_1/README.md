# Homework 1 - DONE
# IMPORTANT: KEEP YOUR REPOSITORIES PUBLIC (DON'T MAKE YOUR REPOSITORY PRIVATE)
# [Submit Homework Form - CLOSED](https://goo.gl/forms/d4n8ycMRxm2d66zX2)
# Deadline: 08.11.2018
# Task
- Using CSS grid layout, create an HTML page that respects the same schema found in the `page_layout.png`;
- General selectors:
    +   `body` should contain all the text color in #00AEEF and white background;
    +   `button` should contain all the text color in #00AEEF;
    +   `hover on button` should have #00AEEF background and white text;
    +   `button` should have 10px border radius;
- Navigation container `css selector: .navigation-container`:
    +   Should have the width of the entire screen;
    +   Should be splitted into 5 equal elements: logo (image or text) and 4 anchors;
- Home container `css selector: .home-container`:
    +   Should have the width of the entire screen;
    +   Should be splitted into 2 equal elements: (text + button) and image;
- Team Container `css selector: .team-container`:
    +   Should have the width of 90% of the screen arranged in the middle;
    +   Should be splitted into 3 equal elements with a space of 20px between each other;
    +   `on hover` each circle should have the following background: #00AEEF #0000FF #FF00FF and white text;
- About Container `css selector: .about-container`:
    +   Should have the width of 90% of the screen arranged in the middle;
    +   Should be splitted into 4 equals elements with a space of 15 px between each other;
    +   `on hover` each card should have #00AEEF background and white text color;
- Contact Container `css selector: .contact-container`:
    +   Should have the width of the entire screen;
    +   Single line of text aligned in the middle;
    +   Font size should be 25px;
