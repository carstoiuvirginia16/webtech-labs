# Project Details WebTech 2018

# [Specifications](https://docs.google.com/document/d/1hSpu35JvciXPjgogAGos_t3CdZGmUhes-9Lgp77PYuc/edit)

# Project Phases

### -   Phase 1 - Deadline 25.11.2018 [Submit Form](https://goo.gl/forms/vswTcU5oSNsSw7in1)
### -   Phase 2 - Deadline 25.12.2018 [Submit Form](https://goo.gl/forms/48GHudSqDt58OJTG2)
### -   Phase 3 - Last Laboratory (DEMO) [Submit Form]()