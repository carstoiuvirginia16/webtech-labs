# Web Technologies Labs:

# Project Details

- [Project Details](https://bitbucket.org/ccartas/webtech-labs/src/master/project/)

# Homework Details

- [Homeworks](https://bitbucket.org/ccartas/webtech-labs/src/master/homeworks)

# Labs Content

- [HTML Basics](https://bitbucket.org/ccartas/webtech-labs/src/master/lab01/);
- [CSS Basics](https://bitbucket.org/ccartas/webtech-labs/src/master/lab02/);
- [Javascript Basics: Part 1](https://bitbucket.org/ccartas/webtech-labs/src/master/lab03/);
- [Javascript Basics: Part 2](https://bitbucket.org/ccartas/webtech-labs/src/master/lab04/);
- [Javascript Basics: Part 3](https://bitbucket.org/ccartas/webtech-labs/src/master/lab05/);
- [Create a CRUD REST API using NodeJS](https://bitbucket.org/ccartas/webtech-labs/src/master/lab06/);
- [Create a CRUD REST API with Persistance Layer](https://bitbucket.org/ccartas/webtech-labs/src/master/lab07/);
- [Todo App with React](https://bitbucket.org/ccartas/webtech-labs/src/master/lab08/);
- [Redux Basics](https://bitbucket.org/ccartas/webtech-labs/src/master/lab09/);